package synthesis.sound;

import javax.swing.SwingUtilities;

public class Main {

	public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                final SynthesisUI ssui = new SynthesisUI("Synthesis Sound");
            }
        });
	}

}
