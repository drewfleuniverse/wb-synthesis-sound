package synthesis.sound;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;

import java.awt.Color;

import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.border.EtchedBorder;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JTextArea;

import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JSpinner;
import javax.swing.JSeparator;
import javax.swing.JSlider;

/*
 * Initial Swing Architecture Design - MVC:
 *   Model - representation of data
 *   View - visual representation of data
 *   Controller - manipulate and update data between model and view
 * Altered Swing Architecture Design - Separable Model architecture:
 *   1. Model
 *   2. UI - coupling view and controller 
 *   
 */

public class SynthesisUI extends JFrame {
	// MenuBar
	private JMenuBar menuBar = new JMenuBar();
	private JMenu mnNewMenu = new JMenu("About");
	private JMenuItem mntmNewMenuItem1 = new JMenuItem("New menu item 1");
	private JMenuItem mntmNewMenuItem2 = new JMenuItem("New menu item 2");
	
	// Container
	private JPanel contentPane;
	
	// Configuration and Real-Time Adjustment
	private JLayeredPane layeredPaneControlPanel = new JLayeredPane();
	private JLabel lblConfiguration = new JLabel("Configuration");
	private JSpinner spinner = new JSpinner();
	private JLabel lblNumOfBaseNodes = new JLabel("Number of Base Nodes");
	private JLabel lblMaxNumOfChild = new JLabel("Maximum Number of Child Nodes");
	private JLabel lblMinNumOfChild = new JLabel("Minimum Number of Child Nodes");
	private JSpinner spinner_1 = new JSpinner();
	private JSpinner spinner_2 = new JSpinner();
	private JSeparator separator = new JSeparator();
	private JLabel lblRealTimeAdjustment = new JLabel("Real-Time Adjustment");
	private JSlider slider = new JSlider();
	private JLabel lblSpeed = new JLabel("Speed");
	private JLayeredPane layeredPaneSelectSignalSource = new JLayeredPane();
	
	// Select Signal Source
	private JLabel lblSelectSignalSource = new JLabel("Select Signal Source");
	private JTextArea textArea_1 = new JTextArea();
	private JButton btnSourceFile1 = new JButton("Source File 1");
	private JButton btnSourceFile2 = new JButton("Source File 2");
	private JTextArea textArea = new JTextArea();
	
	// Synthesized Output
	private JLayeredPane layeredPaneSynthesizedOutput = new JLayeredPane();
	private JLabel lblSynthesizedoutput = new JLabel("SynthesizedOutput");
	private JPanel panelSynthesizedoutput = new JPanel();
	
	// Synthesized Input
	private JLayeredPane layeredPaneInputSignal = new JLayeredPane();
	private JLabel lblInputsignal = new JLabel("InputSignal");
	private JPanel panelInputSignal1 = new JPanel();
	private JPanel panelInputSignal2 = new JPanel();
	
	public SynthesisUI(String title) {
		super(title);
		System.out.println("Synthesis Sound is started");

		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				SynthesisUI.this.setVisible(false);
				SynthesisUI.this.dispose();
				System.out.println("Synthesis Sound is closed");
			}
		});
		

		/*
		 * Setup Base Layout Sections
		 */
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1080, 640);

		setJMenuBar(menuBar);
		menuBar.add(mnNewMenu);
		mnNewMenu.add(mntmNewMenuItem1);
		mnNewMenu.add(mntmNewMenuItem2);

		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		
		/*
		 * Configuration and Real-Time Adjustment
		 */

		layeredPaneControlPanel.setBorder(new EtchedBorder(
				EtchedBorder.LOWERED, null, null));
		layeredPaneControlPanel.setBounds(757, 196, 314, 383);
		contentPane.add(layeredPaneControlPanel);

		lblConfiguration.setBounds(12, 0, 115, 15);
		layeredPaneControlPanel.add(lblConfiguration);

		spinner.setBounds(227, 28, 75, 20);
		layeredPaneControlPanel.add(spinner);

		lblNumOfBaseNodes.setFont(new Font("Dialog", Font.PLAIN, 12));
		lblNumOfBaseNodes.setBounds(12, 30, 157, 15);
		layeredPaneControlPanel.add(lblNumOfBaseNodes);

		lblMaxNumOfChild.setFont(new Font("Dialog", Font.PLAIN, 12));
		lblMaxNumOfChild.setBounds(12, 57, 209, 15);
		layeredPaneControlPanel.add(lblMaxNumOfChild);

		lblMinNumOfChild.setFont(new Font("Dialog", Font.PLAIN, 12));
		lblMinNumOfChild.setBounds(12, 84, 209, 15);
		layeredPaneControlPanel.add(lblMinNumOfChild);
		
		spinner_1.setBounds(227, 55, 75, 20);
		layeredPaneControlPanel.add(spinner_1);

		spinner_2.setBounds(227, 82, 75, 20);
		layeredPaneControlPanel.add(spinner_2);

		separator.setBounds(12, 111, 290, 2);
		layeredPaneControlPanel.add(separator);

		lblRealTimeAdjustment.setBounds(12, 125, 157, 15);
		layeredPaneControlPanel.add(lblRealTimeAdjustment);

		slider.setBounds(12, 179, 290, 26);
		layeredPaneControlPanel.add(slider);

		lblSpeed.setFont(new Font("Dialog", Font.PLAIN, 12));
		lblSpeed.setBounds(12, 152, 70, 15);
		layeredPaneControlPanel.add(lblSpeed);

		/*
		 * Select Signal Source
		 */

		layeredPaneSelectSignalSource.setBorder(new EtchedBorder(
				EtchedBorder.LOWERED, null, null));
		layeredPaneSelectSignalSource.setBounds(757, 12, 314, 172);
		contentPane.add(layeredPaneSelectSignalSource);

		lblSelectSignalSource.setBounds(12, 0, 190, 15);
		layeredPaneSelectSignalSource.add(lblSelectSignalSource);

		textArea_1.setEditable(false);
		textArea_1.setBounds(12, 55, 290, 25);
		layeredPaneSelectSignalSource.add(textArea_1);

		btnSourceFile1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Source File 1 is clicked");
			}
		});
		btnSourceFile1.setBounds(178, 92, 124, 20);
		layeredPaneSelectSignalSource.add(btnSourceFile1);

		btnSourceFile2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Source File 2 is clicked");
			}
		});
		btnSourceFile2.setBounds(178, 124, 124, 20);
		layeredPaneSelectSignalSource.add(btnSourceFile2);

		textArea.setEditable(false);
		textArea.setBounds(12, 28, 290, 25);
		layeredPaneSelectSignalSource.add(textArea);

		/*
		 * Synthesized Output
		 */

		layeredPaneSynthesizedOutput.setBorder(new EtchedBorder(
				EtchedBorder.LOWERED, null, null));
		layeredPaneSynthesizedOutput.setBounds(12, 12, 733, 374);
		contentPane.add(layeredPaneSynthesizedOutput);

		lblSynthesizedoutput.setBounds(12, 0, 173, 15);
		layeredPaneSynthesizedOutput.add(lblSynthesizedoutput);

		// panelSynthesizedoutput.setBackground(new Color(255, 51, 102));
		panelSynthesizedoutput.setBounds(12, 27, 709, 335);
		layeredPaneSynthesizedOutput.add(panelSynthesizedoutput);

		/*
		 * --------------------------------------------------------
		 * Using SynthesisEngine
		 * --------------------------------------------------------
		 */
		execSynEn();

		/*
		 * Synthesized Input
		 */

		layeredPaneInputSignal.setBounds(12, 398, 733, 181);
		contentPane.add(layeredPaneInputSignal);
		layeredPaneInputSignal.setBorder(new EtchedBorder(EtchedBorder.LOWERED,
				null, null));

		lblInputsignal.setBounds(12, 0, 168, 15);
		layeredPaneInputSignal.add(lblInputsignal);

		panelInputSignal1.setBackground(new Color(153, 204, 255));
		panelInputSignal1.setBounds(12, 24, 709, 65);
		layeredPaneInputSignal.add(panelInputSignal1);

		panelInputSignal2.setBackground(new Color(153, 204, 255));
		panelInputSignal2.setBounds(12, 101, 709, 65);
		layeredPaneInputSignal.add(panelInputSignal2);

		
		setVisible(true);
	}
	
	public void execSynEn() {
		// Create SynthesisEngine
		processing.core.PApplet se = new SynthesisEngine(slider);
		panelSynthesizedoutput.add(se);
		se.init();
	}
}
