package synthesis.sound;

import java.net.URL;

import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import ddf.minim.*;
import ddf.minim.analysis.*;
import processing.core.*;

public class SynthesisEngine extends PApplet implements ChangeListener {
	private Minim minim;
	private AudioPlayer jingle;
	private FFT fft;
	private String windowName;
	private int feeder;
	
	
	SynthesisEngine(JSlider slider) {
		slider.addChangeListener(this);
	}
	
	@Override
	public void setup() {
		size(709, 335, P3D);
		minim = new Minim(this);

		URL pUrl = this.getClass().getResource("data/marcus_kellis_theme.mp3");
		System.out.println(pUrl.toString());
		jingle = minim.loadFile(pUrl.toString(), 2048);
		jingle.loop();

		fft = new FFT(jingle.bufferSize(), jingle.sampleRate());
		windowName = "None";
		textFont(createFont("Arial", 16));
	}
	
	@Override
	public void draw() {
		background(80);
		stroke(255, 105, 160);

		fft.forward(jingle.mix);
		for (int i = 0; i < fft.specSize(); i++) {
			line(i, height, i, height - fft.getBand(i) * 4);
		}
		fill(255);
		Runtime rt = Runtime.getRuntime();
		long usedMB = (rt.totalMemory() - rt.freeMemory());
		
		text("The window being used is: " + windowName, 5, 20);
		text( "Memory in use: " + usedMB, 5, 40 );
		text( "External feed in: " + updateFeeder(), 5, 60 );
	}
	
	@Override
	public void keyReleased() {
		if (key == 'w') {
			fft.window(FFT.HAMMING);
			windowName = "Hamming";
		}

		if (key == 'e') {
			fft.window(FFT.NONE);
			windowName = "None";
		}
	}
	
	public void stateChanged(ChangeEvent e) {
		feeder = ((JSlider)e.getSource()).getValue();
	}
	
	private int updateFeeder() {
		return feeder;
	}
	public void foo() {
		;
	}
}
